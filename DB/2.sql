
CREATE TABLE IF NOT EXISTS `api` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `URL` varchar(255) NOT NULL,
  `method` varchar(20) NOT NULL,
  `body` text NOT NULL,
  `headers` text NOT NULL,
  `oauthId` int(11) NOT NULL,
  `success_status_code` int(3) NOT NULL,
  `class_check_details` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `inbound`
--

CREATE TABLE IF NOT EXISTS `inbound` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `api_id` int(5) NOT NULL,
  `ref_uuid` varchar(36) DEFAULT NULL,
  `time_taken` double DEFAULT NULL,
  `inbound_type` varchar(20) DEFAULT NULL,
  `custom` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `init_type` varchar(20) NOT NULL,
  `init_ref` varchar(255) NOT NULL,
  `api_id` int(5) NOT NULL,
  `uuid` varchar(36) DEFAULT NULL,
  `status_code` int(3) NOT NULL,
  `exe_time` double NOT NULL,
  `apicall_status` varchar(20) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `inbound_id` int(5) DEFAULT NULL,
  `error` text,
  `custom_1` text,
  PRIMARY KEY (`id`),
  KEY `uuid` (`uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oauth2`
--

CREATE TABLE IF NOT EXISTS `oauth2` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `accessToken` varchar(255) NOT NULL,
  `refreshToken` varchar(255) NOT NULL,
  `consumerKey` varchar(255) NOT NULL,
  `consumerSecret` varchar(255) NOT NULL,
  `scope` varchar(20) NOT NULL,
  `expire` int(10) NOT NULL,
  `tokenURL` text NOT NULL,
  `lastUpdated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
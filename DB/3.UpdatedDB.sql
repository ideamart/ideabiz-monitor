-- phpMyAdmin SQL Dump
-- version 3.5.8.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 24, 2016 at 07:50 AM
-- Server version: 5.5.48
-- PHP Version: 5.4.45

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `zadmin_apimonitor`
--

-- --------------------------------------------------------

--
-- Table structure for table `ideamartConf`
--

CREATE TABLE IF NOT EXISTS `ideamartConf` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `APP_ID` varchar(20) NOT NULL,
  `PASSWORD` varchar(100) NOT NULL,
  `URL` varchar(100) NOT NULL COMMENT 'https://api.dialog.lk/',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ideamartConf`
--

INSERT INTO `ideamartConf` (`ID`, `APP_ID`, `PASSWORD`, `URL`) VALUES
(1, 'APP_021208', '3625c6a83f9aa8d744d8beddd12efc1c', 'https://api.dialog.lk/');

-- --------------------------------------------------------

--
-- Table structure for table `notificationAddress`
--

CREATE TABLE IF NOT EXISTS `notificationAddress` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `notificationSubscription`
--

CREATE TABLE IF NOT EXISTS `notificationSubscription` (
  `addressId` int(5) NOT NULL,
  `notificationId` int(5) NOT NULL,
  UNIQUE KEY `addressId` (`addressId`,`notificationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `notificationUptimerobot`
--

CREATE TABLE IF NOT EXISTS `notificationUptimerobot` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `monitorId` varchar(50) NOT NULL,
  `monitorName` varchar(100) NOT NULL,
  `notificationClass` varchar(200) NOT NULL,
  `configId` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `monitorId` (`monitorId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

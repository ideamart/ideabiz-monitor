-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 07, 2016 at 12:36 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `monitor`
--

-- --------------------------------------------------------

--
-- Table structure for table `api`
--

CREATE TABLE IF NOT EXISTS `api` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `URL` varchar(255) NOT NULL,
  `method` varchar(20) NOT NULL,
  `body` text NOT NULL,
  `headers` text NOT NULL,
  `oauthId` int(11) NOT NULL,
  `success_status_code` int(3) NOT NULL,
  `class_check_details` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `init_type` varchar(20) NOT NULL,
  `init_ref` varchar(255) NOT NULL,
  `api_id` int(5) NOT NULL,
  `status_code` int(3) NOT NULL,
  `exe_time` double NOT NULL,
  `apicall_status` varchar(20) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `error` text,
  `custom_1` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oauth2`
--

CREATE TABLE IF NOT EXISTS `oauth2` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `accessToken` varchar(255) NOT NULL,
  `refreshToken` varchar(255) NOT NULL,
  `consumerKey` varchar(255) NOT NULL,
  `consumerSecret` varchar(255) NOT NULL,
  `scope` varchar(20) NOT NULL,
  `expire` int(10) NOT NULL,
  `tokenURL` text NOT NULL,
  `lastUpdated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

Check Monitor status
https://cloud.ideabiz.lk/apimonitor/monitor/self

Get Monitor List

https://cloud.ideabiz.lk/apimonitor/data/api/list

Monitor API now
https://cloud.ideabiz.lk/apimonitor/monitor/api/{slug}/?echoOutput=true

param
 echoOutput | true false 	| Print API response  and status code (optional)
 slug		| api slug		|

Sample
https://cloud.ideabiz.lk/apimonitor/monitor/api/smsmessaging-v2
https://cloud.ideabiz.lk/apimonitor/monitor/api/smsmessaging-v2/?echoOutput=true


View Last status
https://cloud.ideabiz.lk/apimonitor/data/status/monitor/{slug}

Sample
https://cloud.ideabiz.lk/apimonitor/data/status/monitor/smsmessaging-v2

View Inbound from ID

https://cloud.ideabiz.lk/apimonitor/data/status/inbound/{inbound id}

sample
https://cloud.ideabiz.lk/apimonitor/data/status/inbound/164553

Inbound only working  for SMS API's for now

View Last status inbound

https://cloud.ideabiz.lk/apimonitor/data/status/monitor/{slug}/inbound

sample
https://cloud.ideabiz.lk/apimonitor/data/status/monitor/smsmessaging-v2/inbound


Last logs
https://cloud.ideabiz.lk/apimonitor/data/last/logs/smsmessaging-v2/{OFFSET-Optional}/{LIMIT}
example
https://cloud.ideabiz.lk/apimonitor/data/last/logs/smsmessaging-v2/0/5
https://cloud.ideabiz.lk/apimonitor/data/last/logs/smsmessaging-v2/5

Last logs from daterange
https://cloud.ideabiz.lk/apimonitor/data/monitor/smsmessaging-v2/{fromdate}/{todate}/{offset}/{limit}
Sample
https://cloud.ideabiz.lk/apimonitor/data/monitor/smsmessaging-v2/2016-10-01/2016-10-01/0/10



to get Mife token, use bellow

http://10.62.96.185:8080/apimonitor/oauth/get/1


generate new
http://10.62.96.185:8080/apimonitor/oauth/generate/3

Refresh
http://10.62.96.185:8080/apimonitor/oauth/refresh/3
package lk.dialog.ideabiz.library;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lk.dialog.ideabiz.model.sms.OutboundSMSMessageRequest;
import lk.dialog.ideabiz.model.sms.OutboundSMSMessagingRequestWrap;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Malinda on 7/13/2015.
 */
public class SendSMS {
    public static String smsEndpoint;
    public static String senderName;
    public static String sourceAddress;
    public static String appId;

    public static Gson gson = null;
    final static Logger logger = Logger.getLogger(SendSMS.class);


    static boolean libloaded = false;

    public static void loadLibrary() {

        Properties prop = new Properties();

        try {
            InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream("configuration/application.properties");
            prop.load(input);
            smsEndpoint = (prop.getProperty("application.sms.SMSEndpoint").toString());
            senderName = (prop.getProperty("application.sms.senderName").toString());
            sourceAddress = (prop.getProperty("application.sms.sourceAddress").toString());
            appId = (prop.getProperty("application.sms.JWTAPPId").toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
        gson = new GsonBuilder().serializeNulls().create();

    }

    public static void sendSMS(String msisdn, String msg, String newSourceAddress, String newSenderName) {

        if (!libloaded) {
            libloaded = true;
            loadLibrary();
        }

        if (newSourceAddress == null || newSourceAddress.length() < 1)
            newSourceAddress = sourceAddress;


        if (newSenderName == null || newSenderName.length() <1)
            newSenderName = senderName;


        Map<String, String> header = new HashMap<String, String>();
        header.put("Content-Type", "application/json");
        header.put("Accept", "application/json");



       // msisdn = "tel:" + NumberFormat.formatNumber(msisdn);

        logger.info("Sending SMS :" + newSenderName + "|" + newSourceAddress+ "|" + msisdn );


        OutboundSMSMessagingRequestWrap smsMessagingRequestWrap = new OutboundSMSMessagingRequestWrap();
        OutboundSMSMessageRequest outboundSMSMessageRequest = new OutboundSMSMessageRequest(msisdn, msg, newSourceAddress, newSenderName);
        smsMessagingRequestWrap.setOutboundSMSMessageRequest(outboundSMSMessageRequest);
        outboundSMSMessageRequest.setClientCorrelator(util.getRandomNumber());
        try {
//            APICall.sendAuthAPICall(1,smsEndpoint, "POST", header, gson.toJson(smsMessagingRequestWrap), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

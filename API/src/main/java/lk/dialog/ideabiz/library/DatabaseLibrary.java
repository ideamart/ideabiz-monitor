package lk.dialog.ideabiz.library;

import javax.sql.DataSource;

/**
 * Created by Malinda on 7/10/2015.
 */
public class DatabaseLibrary {

    /*
    Application Datasource
     */
    private static DataSource APPDataSource;


    public  static DataSource getAPPDataSource() {
        return APPDataSource;
    }

    public  void setAPPDataSource(DataSource APPDataSource) {
        DatabaseLibrary.APPDataSource = APPDataSource;
    }

}

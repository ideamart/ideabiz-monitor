package lk.dialog.ideabiz.library;

import lk.dialog.ideabiz.library.APICall.APICall;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Malinda_07654 on 2/9/2016.
 */
public class LibraryManager {
    final static Logger logger = Logger.getLogger(LibraryManager.class);
    public static APICall apiCall;

    public static APICall getApiCall() {
        return apiCall;
    }

    public void setApiCall(APICall apiCall) {
        this.apiCall = apiCall;
    }


    public LibraryManager() {

    }
}

package lk.dialog.ideabiz.servermonitor;

import com.google.gson.Gson;
import lk.dialog.ideabiz.servermonitor.model.API;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Sri on 29-Dec-15.
 */
@Controller
@RequestMapping("/he/")
public class HeaderEnrichmentController {

    public static Gson gson = null;
    final static Logger logger = Logger.getLogger(HeaderEnrichmentController.class);


    public HeaderEnrichmentController() {
        gson = new Gson();
    }


    @RequestMapping(value = "secure", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public ResponseEntity<String> inbound(HttpServletRequest request) {
        String header = request.getHeader("msisdn");
        if (header == null) {
            return new ResponseEntity<String>("NO HEADER", HttpStatus.OK);
        }
        String UTF = "";

        String body = "ORIGINAL:" + header + "<br>";
        try {
            body += "ISO URL : " + URLEncoder.encode(header, "ISO-8859-1") + "<br>";
        } catch (UnsupportedEncodingException e) {
            logger.error(e.getMessage());
        }
        body += "VALUE UTF : " + toAscii(UTF) + "<br>";


        return new ResponseEntity<String>(body, HttpStatus.OK);
    }

    public static String toAscii(String s) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            sb.append((int) s.charAt(i));
            sb.append(":");
        }
        return sb.toString();
    }

}
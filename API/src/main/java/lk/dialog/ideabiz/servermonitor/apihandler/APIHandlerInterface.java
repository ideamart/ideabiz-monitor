package lk.dialog.ideabiz.servermonitor.apihandler;

import lk.dialog.ideabiz.library.model.APICall.APICallResponse;

/**
 * Created by Malinda_07654 on 1/31/2016.
 */
public interface APIHandlerInterface  {

    public  ReturnStatus processResponse(APICallResponse response,String uuid);
    public  String processRequestBody(String requestBody,String uuid);
    public InboundStatus handleInbound(String apiBody);

}

package lk.dialog.ideabiz.servermonitor.model;

/**
 * Created by Malinda_07654 on 2/8/2016.
 */
public class InboundLog {
    int id;
    String datetime;
    Double timeTaken;
    Integer APIId;
    String refuuid;
    String inboundType;
    String custom;

    public Integer getAPIId() {
        return APIId;
    }

    public void setAPIId(Integer APIId) {
        this.APIId = APIId;
    }

    public String getRefuuid() {
        return refuuid;
    }

    public void setRefuuid(String refuuid) {
        this.refuuid = refuuid;
    }

    public String getInboundType() {
        return inboundType;
    }

    public void setInboundType(String inboundType) {
        this.inboundType = inboundType;
    }

    public String getCustom() {
        return custom;
    }

    public void setCustom(String custom) {
        this.custom = custom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public Double getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(Double timeTaken) {
        this.timeTaken = timeTaken;
    }
}

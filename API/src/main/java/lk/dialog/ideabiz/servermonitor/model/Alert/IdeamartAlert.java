package lk.dialog.ideabiz.servermonitor.model.Alert;

import lk.dialog.ideamart.handler.model.IdeamartAppConfig;
import lk.dialog.ideamart.handler.IdeamartLib;

/**
 * Created by Malinda_07654 on 4/24/2016.
 */
public class IdeamartAlert extends AlertSenderInterface {
    @Override
    public void sendAlert(String destination, String source, String message, Long configId) throws Exception {
        IdeamartLib.sendSMS(destination, message, null, configId);
    }
}

package lk.dialog.ideabiz.servermonitor;

import com.google.gson.Gson;
import lk.dialog.ideabiz.library.LibraryManager;
import lk.dialog.ideabiz.library.model.APICall.APICallResponse;
import lk.dialog.ideabiz.servermonitor.apihandler.APIHandlerInterface;
import lk.dialog.ideabiz.servermonitor.apihandler.InboundStatus;
import lk.dialog.ideabiz.servermonitor.apihandler.ReturnStatus;
import lk.dialog.ideabiz.servermonitor.model.*;
import lk.dialog.ideabiz.servermonitor.model.Alert.AlertSenderInterface;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Sri on 29-Dec-15.
 */
@Controller
@RequestMapping("/uptimerobot/")
public class UptimeRobotController {

    public static Gson gson = null;
    final static Logger logger = Logger.getLogger(UptimeRobotController.class);


    public UptimeRobotController() {
        gson = new Gson();
    }


    @RequestMapping(value = "monitor", method = RequestMethod.GET, produces = "charset=utf-8")
    public ResponseEntity<String> selfMonitor(HttpServletRequest request, @RequestParam String monitorID, @RequestParam Integer alertType) {
        logger.info("Uptime monitor API " + monitorID);

        ArrayList<NotificationUptimerobot> notificationUptimerobots = NotificationLib.getNotificationList(monitorID);

        if (notificationUptimerobots.size() == 0) {
            return new ResponseEntity<String>("NOT_FOUND", HttpStatus.NOT_FOUND);

        }

        String status = "N/A";
        if (alertType == 1) {
            status = "DOWN";
        } else if (alertType == 2) {
            status = "UP";
        }


        for (int x = 0; x < notificationUptimerobots.size(); x++) {

            NotificationUptimerobot notificationUptimerobot = notificationUptimerobots.get(x);
            API api = MonitorLib.getAPIBySlug(null, notificationUptimerobot.getApiId());

            ArrayList<Alarm> alarms = AlarmLib.getAlarm(api.getId(), "ON");
            Alarm alarmOn = null;
            if (alarms.size() > 0)
                alarmOn = alarms.get(0);

            alarms = AlarmLib.getAlarm(api.getId(), "PENDING");
            Alarm alarmPending = null;
            if (alarms.size() > 0)
                alarmPending = alarms.get(0);

            ArrayList<NotificationAddress> notificationAddresses = NotificationLib.getNotificationAddressList(notificationUptimerobot.getId());


            if (status.equalsIgnoreCase("UP")) {
                //if pending, clear
                //send notification

            } else {
                //send warning, notification
                //if pending or ON, ignore
                //else insert pending


            }

            ///old
          //  ArrayList<NotificationAddress> notificationAddresses = NotificationLib.getNotificationAddressList(notificationUptimerobot.getId());


            AlertSenderInterface apiAlertSenderInterface = null;

            try {
                Class<?> notificationClz = null;
                notificationClz = Class.forName(notificationUptimerobot.getNotificationClass());
                apiAlertSenderInterface = (AlertSenderInterface) notificationClz.newInstance();
            } catch (Exception e) {
                logger.error("Notification Alert Claz : " + e.getMessage(), e);
            }


            for (int s = 0; s < notificationAddresses.size(); s++) {
                String sms = NotificationLib.buildNotificationText(notificationUptimerobot.getMonitorName(), notificationAddresses.get(s).getName(), status);
                try {
                    apiAlertSenderInterface.sendAlert(notificationAddresses.get(s).getAddress(), null, sms, notificationUptimerobot.getConfigId());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

        return new ResponseEntity<String>("OK ", HttpStatus.OK);

    }


}
package lk.dialog.ideabiz.servermonitor.model;

/**
 * Created by Sri on 30-Dec-15.
 */
public class API {
    private int id;
    private String name;
    private String slug;
    private String URL;
    private String method;
    private String body;
    private String headers;
    private int oauthId;
    private int success_status_code;
    private String class_check_details;
    Integer alarmGap;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getHeaders() {
        return headers;
    }

    public void setHeaders(String headers) {
        this.headers = headers;
    }

    public int getOauthId() {
        return oauthId;
    }

    public void setOauthId(int oauthId) {
        this.oauthId = oauthId;
    }

    public int getSuccess_status_code() {
        return success_status_code;
    }

    public void setSuccess_status_code(int success_status_code) {
        this.success_status_code = success_status_code;
    }

    public String getClass_check_details() {
        return class_check_details;
    }

    public void setClass_check_details(String class_check_details) {
        this.class_check_details = class_check_details;
    }

    public Integer getAlarmGap() {
        return alarmGap;
    }

    public void setAlarmGap(Integer alarmGap) {
        this.alarmGap = alarmGap;
    }
}

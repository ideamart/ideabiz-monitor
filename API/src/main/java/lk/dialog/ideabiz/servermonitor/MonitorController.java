package lk.dialog.ideabiz.servermonitor;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lk.dialog.ideabiz.library.APICall.APICall;
import lk.dialog.ideabiz.library.LibraryManager;
import lk.dialog.ideabiz.library.model.APICall.APICallResponse;
import lk.dialog.ideabiz.library.model.APICall.OAuth2Model;
import lk.dialog.ideabiz.servermonitor.apihandler.APIHandlerInterface;
import lk.dialog.ideabiz.servermonitor.apihandler.InboundStatus;
import lk.dialog.ideabiz.servermonitor.apihandler.ReturnStatus;
import lk.dialog.ideabiz.servermonitor.model.API;
import lk.dialog.ideabiz.servermonitor.model.APILog;
import lk.dialog.ideabiz.servermonitor.model.InboundLog;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Sri on 29-Dec-15.
 */
@Controller
@RequestMapping("/monitor/")
public class MonitorController {

    public static Gson gson = null;
    public static Gson buetyGson = null;
    final static Logger logger = Logger.getLogger(MonitorController.class);


    public MonitorController() {
        gson = new GsonBuilder().serializeNulls().create();
        buetyGson = new GsonBuilder().setPrettyPrinting().create();
    }


    @RequestMapping(value = "inbound/{apislug}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public ResponseEntity<String> inbound(HttpServletRequest request, @PathVariable String apislug, @RequestBody String body) {
        logger.info("Inbound API : " + apislug);
        logger.debug("Inbound API : " + body);

        API api = MonitorLib.getAPIBySlug(apislug, null);
        if (api == null) {
            return new ResponseEntity<String>("Not Found ", HttpStatus.NOT_FOUND);
        }

        APIHandlerInterface handlerInterface = null;

        if (api.getClass_check_details() != null && api.getClass_check_details().length() > 1) {
            try {
                Class<?> handlerClz = null;
                handlerClz = Class.forName(api.getClass_check_details());
                handlerInterface = (APIHandlerInterface) handlerClz.newInstance();
            } catch (Exception e) {
                logger.error("HandlerClass : " + e.getMessage(), e);
            }
        } else {
            logger.info("No Handler class");
        }

        InboundStatus inboundStatus = null;
        try {
            if (handlerInterface != null)
                inboundStatus = handlerInterface.handleInbound(body);
        } catch (Exception e) {
            logger.error("HandlerClass handleInbound : " + e.getMessage(), e);

        }

        if (inboundStatus != null) {
            int inId = MonitorLib.addInbound(api.getId(), inboundStatus.getRefUuid(), inboundStatus.getTimeTaken(), inboundStatus.getInboundType(), inboundStatus.getCustom());

            if (inboundStatus.getRefUuid() != null)
                MonitorLib.updateLogInboundId(inboundStatus.getRefUuid(), -1, inId);
        }
        return new ResponseEntity<String>("OK ", HttpStatus.OK);

    }

    @RequestMapping(value = "inbound/{apislug}/takentime", method = RequestMethod.GET, produces = "charset=utf-8")
    public ResponseEntity<String> inboundMonitorTakenTime(HttpServletRequest request, @PathVariable String apislug) {
        logger.info("Inbound TakenTime API Monitor : " + apislug);

        API api = MonitorLib.getAPIBySlug(apislug, null);
        if (api == null) {
            return new ResponseEntity<String>("Not Found ", HttpStatus.NOT_FOUND);
        }


        InboundLog inboundLog = MonitorLib.getLastInboundTime(api.getId());

        if (inboundLog == null) {
            return new ResponseEntity<String>("NOT_FOUND ", HttpStatus.NOT_FOUND);

        }
        if (inboundLog.getTimeTaken() > (20 * 1000)) {
            return new ResponseEntity<String>("TIME TAKEN EXCEED 20sec, " + inboundLog.getTimeTaken(), HttpStatus.REQUEST_TIMEOUT);
        }

        return new ResponseEntity<String>("OK ", HttpStatus.OK);

    }

    @RequestMapping(value = "inbound/{apislug}/round", method = RequestMethod.GET, produces = "charset=utf-8")
    public ResponseEntity<String> inboundMonitorTime(HttpServletRequest request, @PathVariable String apislug, @RequestParam(required = false) boolean successOnly) {
        logger.info("Inbound Round API Monitor : " + apislug + " : " + successOnly);

        API api = MonitorLib.getAPIBySlug(apislug, null);
        if (api == null) {
            return new ResponseEntity<String>("Not Found ", HttpStatus.NOT_FOUND);
        }


        ArrayList<APILog> apiLogs = null;
        if (successOnly)
            apiLogs = MonitorLib.getLastLogTime(api.getId(), 1, 1, "MATCH", null);
        else
            apiLogs = MonitorLib.getLastLogTime(api.getId(), 1, 1, null, null);

        APILog lastLog = null;

        if (apiLogs != null && apiLogs.size() > 0)
            lastLog = apiLogs.get(0);

        if (lastLog == null) {
            return new ResponseEntity<String>("NO_CONTENT ", HttpStatus.NO_CONTENT);

        }
        logger.info("Last Log : " + gson.toJson(lastLog));

        try {
            if (lastLog.getInbound_id() == null || lastLog.getInbound_id() == 0) {
                return new ResponseEntity<String>("TIMEOUT ", HttpStatus.REQUEST_TIMEOUT);

            }

            if (!lastLog.getStatus().equalsIgnoreCase("MATCH")) {
                return new ResponseEntity<String>("ERROR ", HttpStatus.INTERNAL_SERVER_ERROR);

            }
            // if(lastLog<inboundTime)

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity<String>("ERROR ", HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return new ResponseEntity<String>("OK ", HttpStatus.OK);

    }


    @RequestMapping(value = "self", method = RequestMethod.GET, produces = "charset=utf-8")
    public ResponseEntity<String> selfMonitor(HttpServletRequest request, String body) {
        logger.info("Self Monitor API ");

        return new ResponseEntity<String>("OK ", HttpStatus.OK);

    }

    @RequestMapping(value = "api/{apislug}", method = RequestMethod.GET, produces = "charset=utf-8")
    public ResponseEntity<String> AppHistory(HttpServletRequest request, @PathVariable String apislug, @RequestParam(required = false) boolean echoOutput) {
        String custom = "";
        String status = "";
        String statusCodeMatch = "";
        String error = "";
        String otherOut = "";
        HttpStatus httpStatus;
        boolean printOutput = false;
        String uuid = UUID.randomUUID().toString().replace("-", "");

        printOutput = echoOutput;


        logger.info("Monitor API : " + apislug);
        API api = MonitorLib.getAPIBySlug(apislug, null);

        if (api == null) {
            return new ResponseEntity<String>("Not Found ", HttpStatus.NOT_FOUND);
        }

        APICallResponse response = null;
        HashMap<String, String> headers = null;
        APIHandlerInterface handlerInterface = null;

        if (api.getClass_check_details() != null && api.getClass_check_details().length() > 1) {
            try {
                Class<?> handlerClz = null;
                handlerClz = Class.forName(api.getClass_check_details());
                handlerInterface = (APIHandlerInterface) handlerClz.newInstance();
            } catch (Exception e) {
                logger.error("HandlerClass : " + e.getMessage(), e);
            }
        } else {
            logger.info("No Handler class");
        }

        try {
            headers = gson.fromJson(api.getHeaders(), new HashMap<String, String>().getClass());
        } catch (Exception e) {
            logger.error("Error Convert header : " + e.getMessage(), e);
        }

        try {
            if (handlerInterface != null)
                api.setBody(handlerInterface.processRequestBody(api.getBody(), uuid));
        } catch (Exception e) {

        }

        if (printOutput) {
            otherOut += "URL : " + api.getURL() + "\n";
            otherOut += "METHOD : " + api.getMethod() + "\n";
            otherOut += "HEADERS : " + gson.toJson(api.getHeaders()) + "\n";
            otherOut += "BODY : " + api.getBody() + "\n";
        }
        try {
            response = LibraryManager.getApiCall().sendAuthAPICall(api.getOauthId(), api.getURL(),
                    api.getMethod(), headers, api.getBody(), false);

            logger.info("Output :" + MonitorLib.getPrint(response.getStatusCode() )+ " | error : " + MonitorLib.getPrint(response.getError()) + " | time  : " + MonitorLib.getPrint(response.getExeTime()) + " | body : " + MonitorLib.getPrintBody(response.getBody()));
            logger.debug("BODY :" + response.getBody());
            if (response.getError() == null) {
                status = "SUCCESS";
                if (api.getSuccess_status_code() == response.getStatusCode()) {
                    statusCodeMatch = "MATCH";
                    httpStatus = HttpStatus.OK;
                } else {
                    status = "ERROR";
                    statusCodeMatch = "NOT_MATCH";
                    httpStatus = HttpStatus.SERVICE_UNAVAILABLE;
                }

                try {
                    if (handlerInterface != null) {
                        ReturnStatus returnStatus = handlerInterface.processResponse(response, uuid);
                        custom = returnStatus.getCustom_1();
                    }
                } catch (Exception e) {
                    logger.error("Handler class : " + e.getMessage(), e);
                }

            } else {
                status = "ERROR";
                error = response.getError().getMessage();
                httpStatus = HttpStatus.SERVICE_UNAVAILABLE;
            }

            if (printOutput) {
                otherOut += "STATUS : " + status + "\n";
                otherOut += "TIME : " + response.getExeTime() + "\n";
                otherOut += "ERROR : " + (response.getError() == null ? "" : response.getError().toString()) + "\n";
                otherOut += "HTTPCODE : " + response.getStatusCode() + "\n";
                otherOut += "BODY : " + response.getBody() + "\n";
            }


        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            MonitorLib.addLog("API", request.getRemoteAddr(), api.getId(), uuid, response.getStatusCode(), response.getExeTime(), "EXCEPTION", "ERROR", e.getMessage(), "");

            if (printOutput) {
                otherOut += "ERROR EXCEPTION : " + (e.getMessage()) + "\n";
            }
            otherOut = otherOut.replaceAll("(\r\n|\n)", "<br/>");
            return new ResponseEntity<String>("ERROR:" + e.getMessage() + "<br/>" + otherOut, HttpStatus.INTERNAL_SERVER_ERROR);

        }
        MonitorLib.addLog("API", request.getRemoteAddr(), api.getId(), uuid, response.getStatusCode(), response.getExeTime(), status, statusCodeMatch, error, custom);

        otherOut = otherOut.replaceAll("(\r\n|\n)", "<br/>");
        return new ResponseEntity<String>(status + "<br>" + otherOut, httpStatus);
    }


}
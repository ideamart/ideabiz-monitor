package lk.dialog.ideabiz.servermonitor;

import lk.dialog.ideabiz.library.DatabaseLibrary;
import lk.dialog.ideabiz.servermonitor.model.*;
import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Properties;

/**
 * Created by Malinda_07654 on 2/7/2016.
 */
public class NotificationLib {
    static Logger logger;
    static String sms;

    public static ArrayList<NotificationUptimerobot> getNotificationList(String id) {
        String sql = "SELECT * FROM `notificationUptimerobot` WHERE  monitorId = ? ";


        ArrayList<NotificationUptimerobot> notificationUptimerobots = new ArrayList<NotificationUptimerobot>();
        Connection dbConnection = null;

        try {

            dbConnection = DatabaseLibrary.getAPPDataSource().getConnection();

            PreparedStatement preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setString(1, id);
            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                NotificationUptimerobot notificationUptimerobot = new NotificationUptimerobot();
                notificationUptimerobot.setId(result.getLong("id"));
                notificationUptimerobot.setMonitorId(result.getString("monitorId"));
                notificationUptimerobot.setMonitorName(result.getString("monitorName"));
                notificationUptimerobot.setNotificationClass(result.getString("notificationClass"));
                notificationUptimerobot.setConfigId(result.getLong("configId"));
                notificationUptimerobot.setApiId(result.getLong("apiId"));
                notificationUptimerobot.setMessage(result.getString("message"));

                notificationUptimerobots.add(notificationUptimerobot);
            }
        } catch (SQLException e) {

            logger.error("DB get notification robot error : " + e.getMessage());

        } finally {
            try {
                if (dbConnection != null && !dbConnection.isClosed())
                    dbConnection.close();
            } catch (SQLException e) {
            }
        }

        return notificationUptimerobots;
    }

    public static ArrayList<NotificationAddress> getNotificationAddressList(Long notificationId) {
        String sql = "SELECT notificationAddress.*,notificationsubscription.notification,notificationsubscription.warning,notificationsubscription.critical FROM `notificationSubscription`,notificationAddress WHERE notificationSubscription.`notificationId` = ? AND notificationAddress.id = `addressId`";


        ArrayList<NotificationAddress> notificationAddresses = new ArrayList<NotificationAddress>();
        Connection dbConnection = null;

        try {

            dbConnection = DatabaseLibrary.getAPPDataSource().getConnection();

            PreparedStatement preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setLong(1, notificationId);
            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                NotificationAddress notificationAddress = new NotificationAddress();
                notificationAddress.setAddress(result.getString("address"));
                notificationAddress.setName(result.getString("name"));
                notificationAddress.setNotification(result.getInt("notification"));
                notificationAddress.setWarning(result.getInt("warning"));
                notificationAddress.setCritical(result.getInt("critical"));
                notificationAddresses.add(notificationAddress);
            }
        } catch (SQLException e) {

            logger.error("DB get notification address error : " + e.getMessage());

        } finally {
            try {
                if (dbConnection != null && !dbConnection.isClosed())
                    dbConnection.close();
            } catch (SQLException e) {
            }
        }

        return notificationAddresses;
    }

    public static String buildNotificationText(String monitorName, String username, String status) {


        String out = sms;
        out = out.replace("##MONITORNAME##", monitorName);
        out = out.replace("##USERNAME##", username);
        out = out.replace("##STATUS##", status);
        return out;
    }

    public NotificationLib() {
        logger = Logger.getLogger(NotificationLib.class);
        Properties props = new Properties();
        Properties log4j = new Properties();
        FileInputStream fis = null;
        try {
            boolean loaded = false;
            try {
                props.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("configuration/application.properties"));
                loaded = true;
            } catch (Exception e) {
                logger.error("Config Load filed from App. trying to load from LIB: ");
            }

            sms = (props.getProperty("application.notification.sms"));


        } catch (Exception e) {

        }
    }


}

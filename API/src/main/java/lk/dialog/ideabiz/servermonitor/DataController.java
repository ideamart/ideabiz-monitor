package lk.dialog.ideabiz.servermonitor;

import com.google.gson.Gson;
import lk.dialog.ideabiz.library.LibraryManager;
import lk.dialog.ideabiz.library.model.APICall.APICallResponse;
import lk.dialog.ideabiz.servermonitor.apihandler.APIHandlerInterface;
import lk.dialog.ideabiz.servermonitor.apihandler.InboundStatus;
import lk.dialog.ideabiz.servermonitor.apihandler.ReturnStatus;
import lk.dialog.ideabiz.servermonitor.model.API;
import lk.dialog.ideabiz.servermonitor.model.APILog;
import lk.dialog.ideabiz.servermonitor.model.InboundLog;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Sri on 29-Dec-15.
 */
@Controller
@RequestMapping("/data/")
public class DataController {

    public static Gson gson = null;
    final static Logger logger = Logger.getLogger(DataController.class);


    public DataController() {
        gson = new Gson();
    }


    @RequestMapping(value = "monitor/{slug}/{from}/{to}/{offset}/{limit}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public ResponseEntity<String> inbound(HttpServletRequest request, @PathVariable String slug, @PathVariable String from, @PathVariable String to, @PathVariable Integer offset, @PathVariable Integer limit) {
        API api = MonitorLib.getAPIBySlug(slug, null);

        if (api == null) {
            return new ResponseEntity<String>("Not Found ", HttpStatus.NOT_FOUND);
        }

        if (!MonitorLib.isThisDateValid(from, "yyyy-MM-dd"))
            return new ResponseEntity<String>("Wrong From date ", HttpStatus.NON_AUTHORITATIVE_INFORMATION);
        if (!MonitorLib.isThisDateValid(to, "yyyy-MM-dd"))
            return new ResponseEntity<String>("Wrong To date ", HttpStatus.NON_AUTHORITATIVE_INFORMATION);


        ArrayList<APILog> logs = MonitorLib.getLogsFromDatetime(api.getId(), from.trim() + " 00:00:00", to.trim() + " 23:59:59", offset, limit);

        if (logs.size() == 0)
            return new ResponseEntity<String>("Data not Found ", HttpStatus.NOT_FOUND);

        return new ResponseEntity<String>(gson.toJson(logs), HttpStatus.OK);

    }

    @RequestMapping(value = "last/logs/{slug}/{offset}/{count}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public ResponseEntity<String> lastLogs2(HttpServletRequest request, @PathVariable String slug, @PathVariable Integer count, @PathVariable Integer offset) {

        API api = MonitorLib.getAPIBySlug(slug, null);

        if (api == null) {
            return new ResponseEntity<String>("Not Found ", HttpStatus.NOT_FOUND);
        }

        ArrayList<APILog> logs = MonitorLib.getLastLogTime(api.getId(), offset, count, null, null);

        if (logs.size() == 0)
            return new ResponseEntity<String>("Not Found ", HttpStatus.NOT_FOUND);

        return new ResponseEntity<String>(gson.toJson(logs), HttpStatus.OK);

    }

    @RequestMapping(value = "last/logs/{slug}/{count}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public ResponseEntity<String> lastLogs(HttpServletRequest request, @PathVariable String slug, @PathVariable Integer count) {

        return lastLogs2(request, slug, count, 0);

    }

    @RequestMapping(value = "status/monitor/{slug}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public ResponseEntity<String> monitorStatus(HttpServletRequest request, @PathVariable String slug) {
        API api = MonitorLib.getAPIBySlug(slug, null);

        if (api == null) {
            return new ResponseEntity<String>("Not Found ", HttpStatus.NOT_FOUND);
        }

        ArrayList<APILog> logs = MonitorLib.getLastLogTime(api.getId(), 0, 1, null, null);

        if (logs.size() == 0)
            return new ResponseEntity<String>("Not Found ", HttpStatus.NOT_FOUND);

        return new ResponseEntity<String>(gson.toJson(logs.get(0)), HttpStatus.OK);

    }

    @RequestMapping(value = "status/monitor/{slug}/inbound", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public ResponseEntity<String> monitorStatusInbound(HttpServletRequest request, @PathVariable String slug) {
        API api = MonitorLib.getAPIBySlug(slug, null);

        if (api == null) {
            return new ResponseEntity<String>("Not Found ", HttpStatus.NOT_FOUND);
        }

        InboundLog logs = MonitorLib.getLastInboundTime(api.getId());

        if (logs == null) {
            return new ResponseEntity<String>("Not Found ", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<String>(gson.toJson(logs), HttpStatus.OK);

    }

    @RequestMapping(value = "status/inbound/{id}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public ResponseEntity<String> monitorInboundId(HttpServletRequest request, @PathVariable Integer id) {


        InboundLog logs = MonitorLib.getInboundById(id);
        if (logs == null) {
            return new ResponseEntity<String>("Not Found ", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<String>(gson.toJson(logs), HttpStatus.OK);

    }


    @RequestMapping(value = "live-log/monitor/{lastId}/{count}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public ResponseEntity<String> liveLog(HttpServletRequest request, Long lastId, Integer count) {

        if (lastId == 0)
            lastId = null;

        return null;
    }


    @RequestMapping(value = "api/list", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public ResponseEntity<String> apiList(HttpServletRequest request) {

        ArrayList<API> apis = MonitorLib.getAPIList();
        return new ResponseEntity<String>(gson.toJson(apis), HttpStatus.OK);

    }


}
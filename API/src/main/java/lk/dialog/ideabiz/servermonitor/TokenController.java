package lk.dialog.ideabiz.servermonitor;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lk.dialog.ideabiz.library.APICall.OAuth2Handler;
import lk.dialog.ideabiz.library.LibraryManager;
import lk.dialog.ideabiz.library.model.APICall.OAuth2Model;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by Sri on 29-Dec-15.
 */
@Controller
@RequestMapping("/oauth/")
public class TokenController {

    public static Gson gson = null;
    final static Logger logger = Logger.getLogger(TokenController.class);


    public TokenController() {
        gson = new GsonBuilder().serializeNulls().create();;
    }


    @RequestMapping(value = "getList", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public ResponseEntity<String> inbound(HttpServletRequest request) {


        return new ResponseEntity<String>("", HttpStatus.OK);
    }

    @RequestMapping(value = "generate/{oauthId}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public ResponseEntity<String> generate(HttpServletRequest request, @PathVariable Integer oauthId) {

        logger.info("GENERATE TOKEN : " + oauthId);
        OAuth2Handler oAuth2Handler = LibraryManager.getApiCall().getoAuth();
        OAuth2Model oAuth2Model = oAuth2Handler.getDataProvider().getToken(oauthId);
        if (oAuth2Model == null) {
            logger.info("Token not found " + oauthId);
            return new ResponseEntity<String>("", HttpStatus.NOT_FOUND);
        }

        logger.info("EXIST TOKEN : " + gson.toJson(oAuth2Model));

        try {
            oAuth2Model = oAuth2Handler.createNewAccessToken(oAuth2Model);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        oAuth2Model.setPassword(maskPassword(oAuth2Model.getPassword()));

        logger.info("NEW TOKEN : " + gson.toJson(oAuth2Model));

        return new ResponseEntity<String>(gson.toJson(oAuth2Model), HttpStatus.OK);
    }

    @RequestMapping(value = "refresh/{oauthId}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public ResponseEntity<String> refresh(HttpServletRequest request, @PathVariable Integer oauthId) {
        logger.info("REFRESH TOKEN : " + oauthId);

        OAuth2Handler oAuth2Handler = LibraryManager.getApiCall().getoAuth();
        OAuth2Model oAuth2Model = oAuth2Handler.getDataProvider().getToken(oauthId);
        if (oAuth2Model == null) {
            logger.info("Token not found " + oauthId);
            return new ResponseEntity<String>("", HttpStatus.NOT_FOUND);
        }

        logger.info("EXIST TOKEN : " + gson.toJson(oAuth2Model));
        try {
            oAuth2Model = oAuth2Handler.createNewAccessToken(oAuth2Model);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        oAuth2Model.setPassword(maskPassword(oAuth2Model.getPassword()));

        logger.info("NEW TOKEN : " + gson.toJson(oAuth2Model));
        return new ResponseEntity<String>(gson.toJson(oAuth2Model), HttpStatus.OK);
    }

    @RequestMapping(value = "get/{oauthId}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public ResponseEntity<String> get(HttpServletRequest request, @PathVariable Integer oauthId) {
        logger.info("GET TOKEN : " + oauthId);

        OAuth2Handler oAuth2Handler = LibraryManager.getApiCall().getoAuth();
        OAuth2Model oAuth2Model = oAuth2Handler.getDataProvider().getToken(oauthId);
        if (oAuth2Model == null) {
            logger.info("Token not found " + oauthId);
            return new ResponseEntity<String>("", HttpStatus.NOT_FOUND);
        }

        oAuth2Model.setPassword(maskPassword(oAuth2Model.getPassword()));
        logger.info("GET TOKEN : " + gson.toJson(oAuth2Model));

        return new ResponseEntity<String>(gson.toJson(oAuth2Model), HttpStatus.OK);
    }

    public String maskPassword(String password){
        if(password == null)
            return null;


        if(password.length()==1)
            return  password;
        if (password.length()==2)
            return password.substring(0,1) + "*";

        if(password.length()<=5)
            return password.substring(0,1) + star(password.length()-2) + password.substring(password.length()-1);

         return password.substring(0,2) + star(password.length()-3) + password.substring(password.length()-1);

    }
    public String star(Integer len){
        StringBuilder sb = new StringBuilder(len);
        for(int i = 0; i < len; i++){
            sb.append('*');
        }
        return sb.toString();
    }


}
package lk.dialog.ideabiz.servermonitor;

import lk.dialog.ideabiz.library.APICall.APICall;
import lk.dialog.ideabiz.library.model.APICall.APICallResponse;
import lk.dialog.ideabiz.library.DatabaseLibrary;
import lk.dialog.ideabiz.servermonitor.model.*;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.log4j.Logger;

/**
 * Created by Malinda_07654 on 2/7/2016.
 */
public class MonitorLib {
    static Logger logger = Logger.getLogger(MonitorLib.class);
    ;

    public static API getAPIBySlug(String slug, Long id) {
        String sql = "SELECT * FROM `api` WHERE ";

        if (slug == null && id != null)
            sql += " `id` = ? ;";
        else
            sql += " `slug` = ? ;";


        API api = null;
        Connection dbConnection = null;

        try {

            dbConnection = DatabaseLibrary.getAPPDataSource().getConnection();

            PreparedStatement preparedStatement = dbConnection.prepareStatement(sql);

            if (slug == null && id != null)
                preparedStatement.setLong(1, id);

            else
                preparedStatement.setString(1, slug);

//            logger.debug(sql);
            logger.debug(preparedStatement.toString());

            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                api = new API();
                api.setId(result.getInt("id"));
                api.setName(result.getString("name"));
                api.setSlug(result.getString("slug"));
                api.setURL(result.getString("URL"));
                api.setMethod(result.getString("method"));
                api.setBody(result.getString("body"));
                api.setHeaders(result.getString("headers"));
                api.setOauthId(result.getInt("oauthId"));
                api.setSuccess_status_code(result.getInt("success_status_code"));
                api.setClass_check_details(result.getString("class_check_details"));
                api.setAlarmGap(result.getInt("alarmGap"));

            }
        } catch (SQLException e) {

            logger.error("DB get API Error : " + e.getMessage());

        } finally {
            try {
                if (dbConnection != null && !dbConnection.isClosed())
                    dbConnection.close();
            } catch (SQLException e) {
            }
        }

        return api;
    }

    public static ArrayList<API> getAPIList() {
        String sql = "SELECT * FROM `api` ";


        ArrayList<API> apis = new ArrayList<API>();
        Connection dbConnection = null;

        try {

            dbConnection = DatabaseLibrary.getAPPDataSource().getConnection();

            PreparedStatement preparedStatement = dbConnection.prepareStatement(sql);
            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                API api = new API();
                api.setId(result.getInt("id"));
                api.setName(result.getString("name"));
                api.setSlug(result.getString("slug"));
                api.setURL(result.getString("URL"));
                api.setMethod(result.getString("method"));
                api.setBody(result.getString("body"));
                api.setHeaders(result.getString("headers"));
                api.setOauthId(result.getInt("oauthId"));
                api.setSuccess_status_code(result.getInt("success_status_code"));
                api.setClass_check_details(result.getString("class_check_details"));
                apis.add(api);
            }
        } catch (SQLException e) {

            logger.error("DB get API Error : " + e.getMessage());

        } finally {
            try {
                if (dbConnection != null && !dbConnection.isClosed())
                    dbConnection.close();
            } catch (SQLException e) {
            }
        }

        return apis;
    }

    public static void updateLogInboundId(String uuid, int logId, int inboundId) {
        String sql = "UPDATE `log` SET `inbound_id` = ? WHERE `log`.`id` = ? OR `log`.`uuid`= ?;";

        Connection dbConnection = null;

        try {

            dbConnection = DatabaseLibrary.getAPPDataSource().getConnection();
            PreparedStatement preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setInt(1, inboundId);
            preparedStatement.setInt(2, logId);
            preparedStatement.setString(3, uuid);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {

            logger.error("DB updateLog for Inbound Error : " + e.getMessage());

        } finally {
            try {
                if (dbConnection != null && !dbConnection.isClosed())
                    dbConnection.close();
            } catch (SQLException e) {
            }
        }
    }

    public static int addInbound(int apiId, String refUuid, double timeTaken, String inboundType, String cuustom) {
        String sql = "INSERT INTO `inbound` (`id`, `datetime`, `api_id`, `ref_uuid`, `time_taken`, `inbound_type`,`custom`) " +
                "VALUES (NULL, CURRENT_TIMESTAMP(), ?, ?, ?, ?,?);;";

        Connection dbConnection = null;
        int id = -1;

        try {

            dbConnection = DatabaseLibrary.getAPPDataSource().getConnection();
            PreparedStatement preparedStatement = dbConnection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, apiId);
            preparedStatement.setString(2, refUuid);
            preparedStatement.setDouble(3, timeTaken);
            preparedStatement.setString(4, inboundType);
            preparedStatement.setString(5, cuustom);
            id = preparedStatement.executeUpdate();


            ResultSet keys = preparedStatement.getGeneratedKeys();

            keys.next();
            id = keys.getInt(1);

        } catch (SQLException e) {
            logger.error("DB add Inbound Error : " + e.getMessage());

        } finally {
            try {
                if (dbConnection != null && !dbConnection.isClosed())
                    dbConnection.close();
            } catch (SQLException e) {
            }
        }

        return id;
    }

    public static void addLog(String init_type, String init_ref, int api_id, String uuid, int status_code, double exe_time, String apicallStatus, String status, String error, String custom_1) {
        String sql = "INSERT INTO `log` (`id`, `datetime`, `init_type`, `init_ref`, `api_id`, `uuid`, `status_code`, `exe_time`,`apicall_status`,`status`, `error`, `custom_1`)" +
                " VALUES (NULL, CURRENT_TIMESTAMP(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

        Connection dbConnection = null;

        try {

            dbConnection = DatabaseLibrary.getAPPDataSource().getConnection();

            PreparedStatement preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setString(1, init_type);
            preparedStatement.setString(2, init_ref);
            preparedStatement.setInt(3, api_id);
            preparedStatement.setString(4, uuid);
            preparedStatement.setInt(5, status_code);
            preparedStatement.setDouble(6, exe_time);
            preparedStatement.setString(7, apicallStatus);
            preparedStatement.setString(8, status);
            preparedStatement.setString(9, error);
            preparedStatement.setString(10, custom_1);
            preparedStatement.executeUpdate();


        } catch (SQLException e) {

            logger.error("DB addlog Error : " + e.getMessage());

        } finally {
            try {
                if (dbConnection != null && !dbConnection.isClosed())
                    dbConnection.close();
            } catch (SQLException e) {
            }
        }


    }

    public static ArrayList<APILog> getLastLogTime(Integer apiId, Integer lastIndex, Integer limit, String status, Long lastId) {
        ArrayList<APILog> apiLogs = new ArrayList<APILog>();

        String sql = "SELECT * FROM `log` WHERE `api_id` =? ";
        if (lastId != null)
            sql += " AND `id`> ? ";
        if (status != null)
            sql += " AND `status`= ? ";

        sql += " ORDER BY id DESC LIMIT ?,?";


        APILog log = null;
        Connection dbConnection = null;

        try {

            dbConnection = DatabaseLibrary.getAPPDataSource().getConnection();

            PreparedStatement preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setInt(1, apiId);

            Integer i = 1;
            if (lastId != null)
                preparedStatement.setLong(++i, lastId);

            if (status != null)
                preparedStatement.setString(++i, status);


            preparedStatement.setInt(++i, lastIndex);
            preparedStatement.setInt(++i, limit);


            //logger.info(preparedStatement);
            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                log = new APILog();
                log.setId(result.getLong("id"));
                log.setDatetime(result.getString("datetime"));
                log.setInit_type(result.getString("init_type"));
                log.setInit_ref(result.getString("init_ref"));
                log.setApi_id(result.getInt("api_id"));
                log.setUuid(result.getString("uuid"));
                log.setStatus_code(result.getInt("status_code"));
                log.setExe_time(result.getDouble("exe_time"));
                log.setApicall_status(result.getString("apicall_status"));
                log.setStatus(result.getString("status"));
                log.setInbound_id(result.getInt("inbound_id"));
                log.setError(result.getString("error"));
                log.setCustom_1(result.getString("custom_1"));
                apiLogs.add(log);
            }
        } catch (SQLException e) {

            logger.error("DB get LastLog Error : " + e.getMessage());

        } finally {
            try {
                if (dbConnection != null && !dbConnection.isClosed())
                    dbConnection.close();
            } catch (SQLException e) {
            }
        }

        return apiLogs;

    }

    public static ArrayList<APILog> getLogsFromDatetime(Integer apiId, String from, String to, Integer offset, Integer limit) {
        ArrayList<APILog> apiLogs = new ArrayList<APILog>();

        String sql = "SELECT * FROM `log` WHERE `api_id` =?  AND datetime >= ? AND datetime <= ? ORDER BY id ASC LIMIT ?,?";


        APILog log = null;
        Connection dbConnection = null;

        try {

            dbConnection = DatabaseLibrary.getAPPDataSource().getConnection();

            PreparedStatement preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setInt(1, apiId);
            preparedStatement.setString(2, from);
            preparedStatement.setString(3, to);

            preparedStatement.setInt(4, offset);
            preparedStatement.setInt(5, limit);


            logger.info(preparedStatement);
            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                log = new APILog();
                log.setId(result.getLong("id"));
                log.setDatetime(result.getString("datetime"));
                log.setInit_type(result.getString("init_type"));
                log.setInit_ref(result.getString("init_ref"));
                log.setApi_id(result.getInt("api_id"));
                log.setUuid(result.getString("uuid"));
                log.setStatus_code(result.getInt("status_code"));
                log.setExe_time(result.getDouble("exe_time"));
                log.setApicall_status(result.getString("apicall_status"));
                log.setStatus(result.getString("status"));
                log.setInbound_id(result.getInt("inbound_id"));
                log.setError(result.getString("error"));
                log.setCustom_1(result.getString("custom_1"));
                apiLogs.add(log);
            }
        } catch (SQLException e) {

            logger.error("DB get datetimelog Error : " + e.getMessage());

        } finally {
            try {
                if (dbConnection != null && !dbConnection.isClosed())
                    dbConnection.close();
            } catch (SQLException e) {
            }
        }

        return apiLogs;

    }

    public static String getLastLogTimeByInboundId(int inid) {
        String sql = "SELECT `datetime` FROM `log` WHERE `inbound_id` =? ORDER BY id DESC LIMIT 0,1";


        String time = null;
        Connection dbConnection = null;

        try {

            dbConnection = DatabaseLibrary.getAPPDataSource().getConnection();

            PreparedStatement preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setInt(1, inid);
            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {

                time = result.getString("datetime");
            }
        } catch (SQLException e) {

            logger.error("DB get LastLog Error : " + e.getMessage());

        } finally {
            try {
                if (dbConnection != null && !dbConnection.isClosed())
                    dbConnection.close();
            } catch (SQLException e) {
            }
        }

        return time;

    }

    public static InboundLog getLastInboundTime(int apiId) {
        String sql = "SELECT * FROM `inbound` WHERE `api_id` =? ORDER BY id DESC LIMIT 0,1";


        Connection dbConnection = null;
        InboundLog inboundLog = null;
        try {

            dbConnection = DatabaseLibrary.getAPPDataSource().getConnection();

            PreparedStatement preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setInt(1, apiId);
            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                inboundLog = new InboundLog();
                inboundLog.setId(result.getInt("id"));
                inboundLog.setDatetime(result.getString("datetime"));
                inboundLog.setTimeTaken(result.getDouble("time_taken"));
                inboundLog.setAPIId(result.getInt("api_id"));
                inboundLog.setRefuuid(result.getString("ref_uuid"));
                inboundLog.setInboundType(result.getString("inbound_type"));
                inboundLog.setCustom(result.getString("custom"));
            }
        } catch (SQLException e) {

            logger.error("DB get LastInbound Error : ", e);

        } finally {
            try {
                if (dbConnection != null && !dbConnection.isClosed())
                    dbConnection.close();
            } catch (SQLException e) {
            }
        }

        return inboundLog;

    }

    public static InboundLog getInboundById(int id) {
        String sql = "SELECT * FROM `inbound` WHERE `id` =? ORDER BY id DESC LIMIT 0,1";


        Connection dbConnection = null;
        InboundLog inboundLog = null;
        try {

            dbConnection = DatabaseLibrary.getAPPDataSource().getConnection();

            PreparedStatement preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                inboundLog = new InboundLog();
                inboundLog.setId(result.getInt("id"));
                inboundLog.setDatetime(result.getString("datetime"));
                inboundLog.setTimeTaken(result.getDouble("time_taken"));
                inboundLog.setAPIId(result.getInt("api_id"));
                inboundLog.setRefuuid(result.getString("ref_uuid"));
                inboundLog.setInboundType(result.getString("inbound_type"));
                inboundLog.setCustom(result.getString("custom"));
            }
        } catch (SQLException e) {

            logger.error("DB get LastInbound Error : ", e);

        } finally {
            try {
                if (dbConnection != null && !dbConnection.isClosed())
                    dbConnection.close();
            } catch (SQLException e) {
            }
        }

        return inboundLog;

    }

    public static boolean isThisDateValid(String dateToValidate, String dateFromat) {

        if (dateToValidate == null) {
            return false;
        }

        SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
        sdf.setLenient(false);

        try {

            java.util.Date date = sdf.parse(dateToValidate);
            System.out.println(date);

        } catch (ParseException e) {

            logger.error("DATEERROR : " + e.getMessage(), e);
            return false;
        }

        return true;
    }

    public MonitorLib() {

    }

    public static String getPrint(String in){
        if(in == null)
            return  "NULL";
        return in;
    }


    public static String getPrint(Exception in){
        if(in == null)
            return  "NULL";
        return in.getMessage();
    }



    public static String getPrint(Long in){
        if(in == null)
            return  "NULL";
        return Long.toString(in);
    }


    public static String getPrint(Integer in){
        if(in == null)
            return  "NULL";
        return Integer.toString(in);
    }



    public static String getPrintBody(String body){
        if(body == null)
            return  "NULL";

        if(body.length()<100)
            return body;

        return body.substring(0,100);
    }

}

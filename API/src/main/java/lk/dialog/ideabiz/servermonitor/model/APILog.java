package lk.dialog.ideabiz.servermonitor.model;

/**
 * Created by Sri on 30-Dec-15.
 */
public class APILog {
    private Long id;
    private String datetime;
    private String init_type;
    private String init_ref;
    private Integer api_id;
    private String uuid;
    private Integer status_code;
    private Double exe_time;
    private String apicall_status;
    private String status;
    private Integer inbound_id;
    private String error;
    private String custom_1;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getInit_type() {
        return init_type;
    }

    public void setInit_type(String init_type) {
        this.init_type = init_type;
    }

    public String getInit_ref() {
        return init_ref;
    }

    public void setInit_ref(String init_ref) {
        this.init_ref = init_ref;
    }

    public Integer getApi_id() {
        return api_id;
    }

    public void setApi_id(Integer api_id) {
        this.api_id = api_id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getStatus_code() {
        return status_code;
    }

    public void setStatus_code(Integer status_code) {
        this.status_code = status_code;
    }

    public Double getExe_time() {
        return exe_time;
    }

    public void setExe_time(Double exe_time) {
        this.exe_time = exe_time;
    }

    public String getApicall_status() {
        return apicall_status;
    }

    public void setApicall_status(String apicall_status) {
        this.apicall_status = apicall_status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getInbound_id() {
        return inbound_id;
    }

    public void setInbound_id(Integer inbound_id) {
        this.inbound_id = inbound_id;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getCustom_1() {
        return custom_1;
    }

    public void setCustom_1(String custom_1) {
        this.custom_1 = custom_1;
    }
}

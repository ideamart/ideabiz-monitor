package lk.dialog.ideabiz.servermonitor.model.Alert;

/**
 * Created by Malinda_07654 on 4/24/2016.
 */
public abstract class AlertSenderInterface {
   public abstract void sendAlert(String destination,String source, String message, Long configId) throws Exception;
}

package lk.dialog.ideabiz.servermonitor.apihandler;

/**
 * Created by Malinda_07654 on 2/7/2016.
 */
public class InboundStatus {
   double timeTaken;
    String refUuid;
    String inboundType;
    String custom;


    public double getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(double timeTaken) {
        this.timeTaken = timeTaken;
    }

    public String getRefUuid() {
        return refUuid;
    }

    public void setRefUuid(String refUuid) {
        this.refUuid = refUuid;
    }

    public String getInboundType() {
        return inboundType;
    }

    public void setInboundType(String inboundType) {
        this.inboundType = inboundType;
    }

    public String getCustom() {
        return custom;
    }

    public void setCustom(String custom) {
        this.custom = custom;
    }
}

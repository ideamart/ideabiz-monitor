package lk.dialog.ideabiz.servermonitor.apihandler.Impliment;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lk.dialog.ideabiz.library.model.APICall.APICallResponse;
import lk.dialog.ideabiz.model.sms.OutboundSMSMessagingRequestWrap;
import lk.dialog.ideabiz.model.sms.InboundSMSRequestWrap;
import lk.dialog.ideabiz.servermonitor.apihandler.APIHandlerInterface;
import lk.dialog.ideabiz.servermonitor.apihandler.InboundStatus;
import lk.dialog.ideabiz.servermonitor.apihandler.ReturnStatus;

/**
 * Created by Malinda_07654 on 1/31/2016.
 */
public class smsv2 implements APIHandlerInterface {
    static Gson gson;
    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(smsv2.class);


    @Override
    public ReturnStatus processResponse(APICallResponse response, String uuid) {
        return new ReturnStatus();
    }

    @Override
    public String processRequestBody(String requestBody, String uuid) {
        OutboundSMSMessagingRequestWrap smsMessagingRequestWrap = gson.fromJson(requestBody, OutboundSMSMessagingRequestWrap.class);
        smsMessagingRequestWrap.getOutboundSMSMessageRequest().getOutboundSMSTextMessage().setMessage("MONIT " + System.currentTimeMillis() + " " + uuid);
        smsMessagingRequestWrap.getOutboundSMSMessageRequest().setClientCorrelator(uuid);
        return gson.toJson(smsMessagingRequestWrap);
    }

    @Override
    public InboundStatus handleInbound(String apiBody) {
        InboundSMSRequestWrap inboundSMSRequestWrap = gson.fromJson(apiBody, InboundSMSRequestWrap.class);
        InboundStatus inboundStatus = new InboundStatus();
        try {
            String sms = inboundSMSRequestWrap.getInboundSMSMessageNotification().getInboundSMSMessage().getMessage();
            String sender = inboundSMSRequestWrap.getInboundSMSMessageNotification().getInboundSMSMessage().getSenderAddress();
            String destination = inboundSMSRequestWrap.getInboundSMSMessageNotification().getInboundSMSMessage().getDestinationAddress();
            String[] part = sms.split(" ");
            inboundStatus.setCustom("SMS:" + sender + "|" + destination+ "|" + sms);
            if (part.length == 3 && part[0].equalsIgnoreCase("MONIT")) {
                inboundStatus.setRefUuid(part[2]);
                inboundStatus.setTimeTaken(System.currentTimeMillis() - Double.parseDouble(part[1]));
                inboundStatus.setInboundType("MONIT");

            } else {
                inboundStatus.setInboundType("KNOWN");
            }


        } catch (Exception e) {
            logger.error("SMS-IN:" + e.getMessage(), e);
        }

        return inboundStatus;
    }

    public smsv2() {
        gson = new GsonBuilder().serializeNulls().create();
    }
}

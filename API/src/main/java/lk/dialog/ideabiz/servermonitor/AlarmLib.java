package lk.dialog.ideabiz.servermonitor;

import lk.dialog.ideabiz.library.DatabaseLibrary;
import lk.dialog.ideabiz.servermonitor.model.Alarm;
import lk.dialog.ideabiz.servermonitor.model.NotificationAddress;
import lk.dialog.ideabiz.servermonitor.model.NotificationUptimerobot;
import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;

/**
 * Created by Malinda_07654 on 2/7/2016.
 */
public class AlarmLib {
    static Logger logger;
    static String sms;

    public static ArrayList<Alarm> getAlarm(Integer APIId, String status) {
        String sql = "SELECT * FROM `alarm` WHERE `status` = ? ";

        if (APIId != null)
            sql += " AND  `api` = ? ";


        ArrayList<Alarm> alarms = new ArrayList<Alarm>();
        Connection dbConnection = null;

        try {

            dbConnection = DatabaseLibrary.getAPPDataSource().getConnection();

            PreparedStatement preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setString(1, status);
            if (APIId != null)
                preparedStatement.setInt(2, APIId);

            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                Alarm alarm = new Alarm();
                alarm.setId(result.getLong("id"));
                alarm.setApi(result.getLong("Alarm"));
                alarm.setCreatedTime(result.getString("createdTime"));
                alarm.setStartingTime(result.getString("startingTime"));
                alarm.setStartTime(result.getString("startTime"));
                alarm.setOffTime(result.getString("offTime"));
                alarm.setCreatedBy(result.getString("createdBy"));
                alarm.setOffBy(result.getString("offBy"));
                alarm.setStatus(result.getString("status"));


                alarms.add(alarm);
            }
        } catch (SQLException e) {

            logger.error("DB get alarm  error : " + e.getMessage());

        } finally {
            try {
                if (dbConnection != null && !dbConnection.isClosed())
                    dbConnection.close();
            } catch (SQLException e) {
            }
        }

        return alarms;
    }

    public static void addAlarm(Alarm alarm) {
        String sql = "INSERT INTO `alarm` (`id`, `api`, `createdTime`, `startingTime`, `startTime`, `offTime`, `createdBy`, `offBy`, `status`) " +
                "VALUES (NULL, ?, CURRENT_TIMESTAMP(), ?, ?, ?, ?, ?, ?);";


        Connection dbConnection = null;

        try {

            dbConnection = DatabaseLibrary.getAPPDataSource().getConnection();

            PreparedStatement preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setLong(1, alarm.getApi());
            preparedStatement.setString(2, alarm.getStartingTime());
            preparedStatement.setString(3, alarm.getStartTime());
            preparedStatement.setString(4, alarm.getOffTime());
            preparedStatement.setString(5, alarm.getCreatedBy());
            preparedStatement.setString(6, alarm.getOffBy());
            preparedStatement.setString(7, alarm.getStatus());

            preparedStatement.executeUpdate();


        } catch (SQLException e) {

            logger.error("DB add alarm  error : " + e.getMessage());

        } finally {
            try {
                if (dbConnection != null && !dbConnection.isClosed())
                    dbConnection.close();
            } catch (SQLException e) {
            }
        }
    }

    public static void updateAlarm(Alarm alarm) {
        String sql = "UPDATE `alarm` " +
                "SET `createdTime` = ?, `startingTime` = ?, `startTime` = ? , `offTime` = ?, `createdBy` = ?, `offBy` = ?, `status` = ? " +
                "WHERE `alarm`.`id` = ?;";


        Connection dbConnection = null;

        try {

            dbConnection = DatabaseLibrary.getAPPDataSource().getConnection();

            PreparedStatement preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setString(1, alarm.getCreatedTime());
            preparedStatement.setString(2, alarm.getStartingTime());
            preparedStatement.setString(3, alarm.getStartTime());
            preparedStatement.setString(4, alarm.getOffTime());
            preparedStatement.setString(5, alarm.getCreatedBy());
            preparedStatement.setString(6, alarm.getOffBy());
            preparedStatement.setString(7, alarm.getStatus());
            preparedStatement.setLong(8, alarm.getId());

            preparedStatement.executeUpdate();


        } catch (SQLException e) {

            logger.error("DB update alarm  error : " + e.getMessage());

        } finally {
            try {
                if (dbConnection != null && !dbConnection.isClosed())
                    dbConnection.close();
            } catch (SQLException e) {
            }
        }


    }

    public static String addSecToCurrent(Integer sec) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, sec);

        SimpleDateFormat sdf =
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        return sdf.format(calendar);
    }


}

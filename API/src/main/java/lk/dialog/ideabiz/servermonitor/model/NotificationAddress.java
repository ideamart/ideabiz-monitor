package lk.dialog.ideabiz.servermonitor.model;

import com.sun.org.apache.xpath.internal.operations.Bool;

/**
 * Created by Malinda_07654 on 4/24/2016.
 */
public class NotificationAddress {
    String name;
    String address;
    Boolean notification;
    Boolean warning;
    Boolean critical;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getNotification() {
        return notification;
    }

    public void setNotification(Boolean notification) {
        this.notification = notification;
    }

    public void setNotification(Integer notification) {
        this.notification = (notification == 1);
    }

    public Boolean getWarning() {
        return warning;
    }

    public void setWarning(Boolean warning) {
        this.warning = warning;
    }

    public void setWarning(Integer warning) {
        this.warning = (warning == 1);
    }

    public Boolean getCritical() {
        return critical;
    }

    public void setCritical(Boolean critical) {
        this.critical = critical;
    }

    public void setCritical(Integer critical) {
        this.critical = (critical == 1);
    }


}

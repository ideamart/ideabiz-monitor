package lk.dialog.ideabiz.model.common;

import java.util.Objects;

/**
 * Created by Malinda on 7/10/2015.
 */
public class Response {


    private String statusCode;
    private String message;
    private  Object data;

    public Response(String statusCode, String message,Object data) {
        this.statusCode = statusCode;
        this.message = message;
        this.data = data;
    }

    public Response(String statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
        this.data = null;
    }


    public String getStatusCode() {
        return this.statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}

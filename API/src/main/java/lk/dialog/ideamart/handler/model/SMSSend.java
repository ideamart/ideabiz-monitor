package lk.dialog.ideamart.handler.model;

import java.util.ArrayList;

/**
 * Created by Malinda_07654 on 4/24/2016.
 */
public class SMSSend {
    String message;
    ArrayList<String> destinationAddresses;
    String password;
    String applicationId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<String> getDestinationAddresses() {
        return destinationAddresses;
    }

    public void setDestinationAddresses(ArrayList<String> destinationAddresses) {
        this.destinationAddresses = destinationAddresses;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }
}

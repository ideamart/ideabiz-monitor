package lk.dialog.ideamart.handler;


import com.google.gson.Gson;
import lk.dialog.ideabiz.servermonitor.model.API;
import lk.dialog.ideamart.handler.model.APICallResponse;
import org.apache.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

/**
 * Created by Malinda_07654 on 4/24/2016.
 */
public class APICall {
    public static int timeout = 10000;
    static Logger logger = Logger.getLogger(APICall.class);
    static  Gson gson = new Gson();

    public static APICallResponse runAPICall(String url, String method, Map<String, String> headers, String body) throws Exception {
        APICallResponse r = new APICallResponse();

        //this for calculate execution time
        long startTime = System.currentTimeMillis();

        try {
            logger.info("Sending request to URL : " + url);
            logger.debug("Post parameters : " + body);
            logger.debug("Headers : " + gson.toJson(headers));

            URL obj = new URL(url);
            HttpURLConnection con = null;

            //set HTTP or HTTPS
            if (url.startsWith("https"))
                con = (HttpsURLConnection) obj.openConnection();
            else
                con = (HttpURLConnection) obj.openConnection();

            //Set params
            con.setConnectTimeout(timeout);
            con.setReadTimeout(timeout);
            con.setRequestMethod(method);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");


            //setting headers
            try {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    con.setRequestProperty(entry.getKey(), entry.getValue());
                }
            } catch (Exception e) {

            }

            //Send body if PUT or POST
            if ((method.equalsIgnoreCase("POST") || method.equalsIgnoreCase("PUT")) && body != null) {
                try {
                    con.setDoOutput(true);
                    DataOutputStream wr = null;

                    wr = new DataOutputStream(con.getOutputStream());

                    wr.writeBytes(body);
                    wr.flush();
                    wr.close();
                } catch (IOException e1) {

                }
            }


            int responseCode = con.getResponseCode();
            logger.info("Response Code : " + responseCode + " :  " + url);

            BufferedReader in;
            if (responseCode >= 400) {
                in = new BufferedReader(
                        new InputStreamReader(con.getErrorStream(), "UTF-8"));
            } else {
                in = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "UTF-8"));
            }


            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            logger.debug("Response  : " + response.toString() + " : " + url);

            r.setBody(response.toString());
            r.setStatusCode(responseCode);


        } catch (Exception e) {
            logger.error("API CALL FAILED " + e.getMessage() + " : " + url);
            r.setError(e);
        }

        long duration = (System.currentTimeMillis() - startTime);
        r.setExeTime(duration);

        return r;
    }

}

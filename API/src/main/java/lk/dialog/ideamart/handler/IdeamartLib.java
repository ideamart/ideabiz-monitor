package lk.dialog.ideamart.handler;

import com.google.gson.Gson;
import lk.dialog.ideabiz.library.DatabaseLibrary;
import lk.dialog.ideabiz.servermonitor.model.API;
import lk.dialog.ideamart.handler.model.IdeamartAppConfig;
import lk.dialog.ideamart.handler.model.SMSSend;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Malinda_07654 on 4/24/2016.
 */
public class IdeamartLib {
    static Logger logger = Logger.getLogger(IdeamartLib.class);
    static Gson gson = new Gson();

    public static IdeamartAppConfig getAppInfo(Long appId) {
        String sql = "SELECT * FROM `ideamartConf` WHERE `id` = ?";


        IdeamartAppConfig ideamartAppConfig = null;
        Connection dbConnection = null;

        try {

            dbConnection = DatabaseLibrary.getAPPDataSource().getConnection();

            PreparedStatement preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setLong(1, appId);
            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                ideamartAppConfig = new IdeamartAppConfig();
                ideamartAppConfig.setID(result.getLong("ID"));
                ideamartAppConfig.setAPP_ID(result.getString("APP_ID"));
                ideamartAppConfig.setPASSWORD(result.getString("PASSWORD"));
                ideamartAppConfig.setURL(result.getString("URL"));
            }
        } catch (SQLException e) {

            logger.error("DB Ideamart Error : " + e.getMessage());

        } finally {
            try {
                if (dbConnection != null && !dbConnection.isClosed())
                    dbConnection.close();
            } catch (SQLException e) {
            }
        }

        return ideamartAppConfig;
    }

    public static void sendSMS(String destination, String message, String alias, Long confId) throws Exception {
        IdeamartAppConfig ideamartAppConfig = IdeamartLib.getAppInfo(confId);

        SMSSend smsSend = new SMSSend();
        smsSend.setApplicationId(ideamartAppConfig.getAPP_ID());
        smsSend.setMessage(message);
        smsSend.setPassword(ideamartAppConfig.getPASSWORD());

        ArrayList<String> destinations = new ArrayList<String>();
        if (!destination.startsWith("tel:"))
            destination = "tel:" + destination;
        destinations.add(destination);
        smsSend.setDestinationAddresses(destinations);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");

        try {
            APICall.runAPICall(ideamartAppConfig.getURL() + "sms/send", "POST", headers, gson.toJson(smsSend));
        } catch (Exception e) {
            logger.error("APICall error ", e);
            throw e;
        }

    }
}
